<?php

return [

    'steps' => [
        'step1' => ['precentage' => 0],
        'step2' => ['precentage' => 20],
        'step3' => ['precentage' => 40],
        'step4' => ['precentage' => 50],
        'step5' => ['precentage' => 70],
        'step6' => ['precentage' => 90],
        'step7' => ['precentage' => 99],
        'step8' => ['precentage' => 100]
    ]

];
