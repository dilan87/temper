<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create ( 'users', function ($table) {
            $table->bigIncrements('user_id');
            $table->dateTime('created_at');
            $table->integer ( 'onboarding_perentage' );
            $table->integer ( 'count_applications' );
            $table->integer ( 'count_accepted_applications' );

        } );


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop ( 'users' );
    }
}
