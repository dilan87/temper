<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen ( base_path () . '/csv/export.csv', 'r' )) !== FALSE) {
            while ( ($data = fgetcsv ( $handle, 1000, ',' )) !== FALSE ) {

                DB::table('users')->insert([
                    'user_id' => $data [0],
                    'created_at' => $data [1],
                    'onboarding_perentage' => $data [2],
                    'count_accepted_applications  ' => $data [3],
                ]);
            }
            fclose ( $handle );
        }else{dd($handle);}
    }
}
