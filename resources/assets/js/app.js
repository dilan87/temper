/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue';
import VueHighcharts from 'vue-highcharts';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

Vue.use(VueHighcharts);

var options = {
    chart: {
        type: 'spline'
    },
    plotOptions: {
        series: {
            marker: {
                enabled: false
            }
        }
    },
    title: {
        text: 'On-boarding process retention curve',
        x: -20 //center
    },
    subtitle: {
        text: 'Weekly insight of how users flow through the onboarding process.',
        x: -20
    },
    xAxis: {

        title: {
            text: 'Steps in the On-boarding process'
        },
        categories: ['Step1', 'Step2', 'Step3', 'Step4', 'Step5', 'Step6',
            'Step7', 'Step8', 'Step9', 'Step10', 'Step11', 'Step12'
        ]
    },
    yAxis: {
        min: 0,
        max: 100,
        labels: {
            format: '{value}%'
        },
        valueSuffix: '%',
        title: {
            text: 'Completed Precentage'
        },
        plotLines: [{
            value: 0,
            width: 1,
            color: '#808080'
        }]
    },
    tooltip: {
        valueSuffix: '%'
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
    },
    series: []
};

const app = new Vue({
    el: '#app',
    data: {
        options: options
    },
    created: function()
    {
        this.fetchRetentiondata();
    },
    methods: {
        fetchRetentiondata: function() {
            axios.get('/api/weekly_retention').then(function (response) {
                 app.options.series = response.data;
                console.log(app.options.series);
            });
        }
    }
});


