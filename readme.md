## About Retention Sample App

this app is created as a sample implementation to get insights of user retention 
over perioid of time.

## Prerequisites

- Composer
- MySQL server
- PHP 7.0+


## Installation
- create a database in your mysql server

After cloning this repository,  go to source folder and do the following...

-  Rename .env.example to .env and modify it and provide your database access details there. you need to have an empty database created

   - run the following command/s,
     - composer install
     - composer update
     - npm install
     - npm run dev
     - php artisan migrate
     - php artisan db:seed
     - php artisan key:generate
     - php artisan serve
     
App running on Laravel Development server will be available to access on http://127.0.0.1:8000 

## Unit Testing
Test cases are available in ./tests/feature/RetentionTest.php
To execute the Test cases run the following command on source root directory
   - vendor\bin\phpunit

## Coding Standards
I have maintained PSR-2 standards while implementing this app.

To execute PHP code sniffer, run this command on app root directory:


- vendor\bin\phpcs --standard=PSR2 app 


## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

VueJS framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
