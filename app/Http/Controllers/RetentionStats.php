<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RetentionStats extends Controller
{
    private $users;

    /**
     * constructor for the controller. implemented to handle model dependency injection
     * RetentionStats constructor.
     * @param Users $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }

    /**
     * This function is used to get all the users in the user table
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllUsers()
    {
        return $this->users->all();
    }

    /**
     * This function is used to generate weekly retention statistics for retention curve, to be used for highcharts
     * @return array
     */
    public function getWeeklyRetention()
    {
        $firstUser = $this->users->get()->first();
        $lastUser = $this->users->get()->last();
        $lastUserCreatedDate = $lastUser->created_at;
        $startDate = $firstUser->created_at;
        $endDate = $firstUser->created_at->addDays(6);
        $retentionCurves = array();
        while ($startDate->lt($lastUserCreatedDate)) {
            $steps = array();
            $totalUsers = $this->users->whereBetween(
                'created_at',
                array($startDate->toDateTimeString(), $endDate->toDateTimeString())
            )->count();

            for ($x = 1; $x < 9; $x++) {
                $num = $this->getNumberOfUsersByCompletionProgress(
                    config('onboarding.steps.step' . $x),
                    $startDate->toDateTimeString(),
                    $endDate->toDateTimeString()
                );
                array_push($steps, round(($num / $totalUsers) * 100));
            }
            $curve = array();
            $curve['name'] = 'Week (' . $startDate->format('M d Y') . ')';
            $curve['data'] = $steps;
            array_push($retentionCurves, $curve);

            $startDate = $startDate->addDays(7);
            $endDate = $endDate->addDays(6);
        }

        return $retentionCurves;
    }

    /**
     * This function is used to get total number of users for the specified
     * @param $precentage
     * @param $startDate
     * @param $endDate
     * @return mixed
     */
    private function getNumberOfUsersByCompletionProgress($precentage, $startDate, $endDate)
    {
        $result = $this->users->where('onboarding_perentage', '>=', $precentage)->whereBetween(
            'created_at',
            array($startDate, $endDate)
        )->count();
        return $result;
    }
}
