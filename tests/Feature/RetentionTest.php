<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RetentionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testWeeklyRetentionAPI()
    {
        $response = $this->get('/api/weekly_retention');
        $response->assertStatus(200)->assertJson([]);
    }
}
